__author__ = 'alexander'


import os
import sys
import logging
from methods.mhash import MHash
from methods.newparc import MPReg

logger = logging.getLogger()

poickREG = []
key = ''
reg = ''
WinListSys = []
OtherList = []
ProgList = []
ProgUninstallList = []
ProgRunList = []


'''
Logger init
'''
def initialize_logger(output_dir):

    logger.setLevel(logging.DEBUG)

    # create console handler and set level to info
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create error file handler and set level to error
    handler = logging.FileHandler(os.path.join(output_dir, "error.log"), "a")
    handler.setLevel(logging.ERROR)
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create debug file handler and set level to debug
    handler = logging.FileHandler(os.path.join(output_dir, "debug.log"), "w")
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)


'''
main function
'''
def main():
    initialize_logger('log')
    if len(sys.argv) == 2:
        base_dir = sys.argv[1]
        m_hash = MHash(logger=logger, db='m_hash.db')

        mp_reg = MPReg(poickREG=poickREG, key=key, reg=reg, WinListSys=WinListSys,
                       ProgRunList=ProgRunList, ProgUninstallList=ProgUninstallList,
                       OtherList=OtherList, ProgList=ProgList, base_dir=base_dir, logger=logger)
        m_hash.check(dir=base_dir, res_file='result/m_hash')
        mp_reg.check()
    else:
        logger.error('No start directory specified')


if __name__ == "__main__":
    main()
