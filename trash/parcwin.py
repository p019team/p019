import sys
from Registry import Registry
from poickreg import MsREG
from methods.base import MBase

"""def findin():
    input = open('result/proba', 'r')
    findfile = '/System32/config/SOFTWARE'
    poick = []
    for line in input:
        find = line.find(findfile)
        endd = line.find('\n')
        if find != -1:
            poick.append(line[:endd])

    input.close
    return poick"""


class MPRegWin(MBase):
    def check(self, **kwargs):
        print "dd"

    def __init__(self, **kwargs):
        self.mspoickreg = MsREG(logger=kwargs.get('logger'))
        self.logger = kwargs.get('logger')
        self.programs = kwargs.get('programs')
        self.poickREG = kwargs.get('poickREG')
        self.CurrentWin = kwargs.get('CurrentWin')

    def WinCur(self, **kwargs):
        f = self.poickREG
        for line in f[0]:
            reg = Registry.Registry(line)

            try:
                key = reg.open("Microsoft\\Windows NT\\CurrentVersion")
            except Registry.RegistryKeyNotFoundException:
                self.logger.error("Couldn't find Run key. Exiting...")
                sys.exit(-1)

            for value in [v for v in key.values() \
                          if v.value_type() == Registry.RegSZ or \
                                          v.value_type() == Registry.RegExpandSZ]:
                print "%s: %s" % (value.name(), value.value())
                self.CurrentWin.write("%s: %s" % (value.name(), value.value()))
