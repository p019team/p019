import os
import glob



class MsREG():

    def __init__(self, **kwargs):
        self.logger = kwargs.get('logger')
        self.poickREG = kwargs.get('poickREG')

    def findin(self, findfile):
        os.chdir('/')
        s = 0
        while s != 7:
            poick = glob.glob(findfile)
            for simbol in poick:
                if simbol != "":
                    s = 7
                else:
                    findfile = '/*' + findfile
                    s = s + 1

        if poick is None:
            self.logger.error("NOT FIND " + findfile[14:])
        return poick

    def check(self, **kwargs):
        start = os.getcwd()

        regfilesSoftware = '/System32/config/SOFTWARE'
        regfilesSystem = 'System32/config/SYSTEM'
        regfilesSecurity ='System32/config/SECURITY'
        regfilesSam = 'System32/config/SAM'
        regfilesHkey_usersdefault = 'System32/config/DEFAULT'
        regfilesHkey_usersS1518 = 'System32/config/systemprofile/NTUSER.DAT'
        regfilesHkey_usersS1519 = 'ServiceProfiles/LocalService/NTUSER.DAT'
        regfilesHkey_usersS1520 = 'ServiceProfiles/NetworkService/NTUSER.DAT'
        regfilesHkey_usersClasses = 'AppData/Local/Microsoft/Windows/UsrClass.dat'
        regfiles = '/NTUSER.DAT'
        regfilesHkey_usersUsers = []

        poickSoftware = self.findin(regfilesSoftware)
        poickSystem = self.findin(regfilesSystem)
        poickSecurity = self.findin(regfilesSecurity)
        poickSam = self.findin(regfilesSam)
        poickHkey_usersdefault = self.findin(regfilesHkey_usersdefault)
        poickHkey_usersS1518 = self.findin(regfilesHkey_usersS1518)
        poickHkey_usersS1519 = self.findin(regfilesHkey_usersS1519)
        poickHkey_usersS1520 = self.findin(regfilesHkey_usersS1520)
        poickHkey_usersClasses = self.findin(regfilesHkey_usersClasses)


        if poickHkey_usersClasses != []:
            for users in poickHkey_usersClasses:
                f = users.find('/AppData/Local/Microsoft/Windows/')
                users = users[:f]
                f = users.rfind('/')
                users = users[f:] + regfiles
                regfilesHkey_usersUsers.append(users)

        for user in regfilesHkey_usersUsers:
            poickHkey_userUsers = self.findin(user)

        self.poickREG = [poickSoftware, poickSystem, poickSecurity, poickSam, poickHkey_usersdefault,\
                          poickHkey_usersS1518, poickHkey_usersS1519, poickHkey_usersS1520, poickHkey_usersClasses,\
                         poickHkey_userUsers]

        os.chdir(start)