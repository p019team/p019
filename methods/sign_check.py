__author__ = 'pavel'


# Function for signature verification
def signCheck(filePath):
    """
    example for filePath: C:\Python27\app.exe
    """
    str = ''
    f = open(filePath, 'rb')
    text = f.read()
    index = text.find("PE")
    peFile = 1
    if index > 0 and index > -1:
        index = index + 152  #If PE file - jump to signature initialisation
        f.seek(index)
        i = 4
        flag = 0
        while i != 0:
            text = f.read(1)
            if (' '.join(format(ord(x), 'b') for x in text)) != '0':
                f.read(i - 1)
                i = 1
                flag = 1
                str = "has a digital signature."
            i = i - 1
        if flag == 0:
            str = "doesn't have a signature."
        i = 4
        if flag == 1:
            while i != 0:
                text = f.read(1)
                if (' '.join(format(ord(x), 'b') for x in text)) != '0':
                    f.read(i - 1)
                    i = 1
                    flag = 0
                    str = str + " It is normal."
                i = i - 1
        if flag == 1:  #dead link to signature
            str = str + " Signature has been damaged."
    else:
        str = str + "is not PE file."
    f.close
    return str
