from __future__ import print_function
__author__ = 'alexander'

import hashlib
import fnmatch
import os
import sys
import sqlite3
import operator
import collections
import sign_check
from base import MBase

'''
Programs detection by hash sum of executable
'''
class MHash(MBase):
    ext = [
        'exe',
        'dll',
        'py',
    ]

    def __init__(self, **kwargs):
        self.logger = kwargs.get('logger')
        self.conn = sqlite3.connect(kwargs.get('db'))
        self.init_db()

    '''
    Inits DB
    '''
    def init_db(self):
        c = self.conn.cursor()
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS executable (
            program text,
            version text,
            hash text,
            is_forbidden int,
            forbidden int
            )
            '''
        )

    '''
    Returns hash sum of the file by the given path
    '''
    @staticmethod
    def hash_file(path):
        blocksize = 128
        hasher = hashlib.md5()
        with open(path, 'rb') as afile:
            buf = afile.read(blocksize)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(blocksize)
        return hasher.hexdigest()

    '''
    Adds exe to the DB
    '''
    def add_exe(self, path, program, version):
        h = self.hash_file(path)
        c = self.conn.cursor()
        try:
            c.execute(
                '''
                INSERT INTO executable
                VALUES (?, ?, ?, 0, 0)
                ''',
                (program, version, h)
            )
            self.conn.commit()
        except sqlite3.Error as e:
            print(e.message)

    '''
    Searches exe info in DB
    '''
    def check_exe(self, path):
        h = self.hash_file(path)
        c = self.conn.cursor()
        try:
            c.execute(
                '''
                SELECT *
                FROM executable
                WHERE hash = ?
                ''',
                (h, )
            )
            return c
        except sqlite3.Error as e:
            print(e.args[0])

    '''
    Looks up given dir and adds all exes in the dir
    '''
    def add_exe_in_dir(self, directory):
        for root, dirnames, filenames in os.walk(directory):
            for e in self.ext:
                for filename in fnmatch.filter(filenames, '*.' + e):
                    file_path = os.path.join(root, filename)
                    print(file_path)
                    s = sys.stdin.readline()
                    s = s[:len(s) - 1]
                    s2 = sys.stdin.readline()
                    s2 = s2[:len(s2) - 1]
                    if s != '':
                        self.add_exe(path=file_path, program=s, version=s2)

    '''
    Standard entry point of the method
    '''
    def check(self, **kwargs):
        try:
            programs = collections.defaultdict(int)
            unknown = []
            for root, dirnames, filenames in os.walk(kwargs.get('dir')):
                for e in self.ext:
                    for filename in fnmatch.filter(filenames, '*.' + e):
                        file_path = os.path.join(root, filename)
                        print(file_path)
                        rows = self.check_exe(file_path)
                        if rows is not None:
                            rows2 = rows.fetchall()
                            number_of_rows = len(rows2)
                        else:
                            number_of_rows = 0
                        if number_of_rows > 0:
                            for row in rows2:
                                programs[str(row[4]) + ' ' + row[0] + ' ' + row[1] + ' ' + sign_check.signCheck(file_path)] += 10
                        else:
                            unknown.append(sign_check.signCheck(file_path) + ' ' + file_path)

            sorted_programs = sorted(programs.items(), key=operator.itemgetter(1), reverse=True)
            res_file = open(kwargs.get('res_file'), 'w')
            for p in sorted_programs:
                print('%70s\t|\t%s' % (p[0], str(p[1])), file=res_file)
            res_file.close()
            res_file = open(kwargs.get('res_file') + '.unknwn', 'w')

            for p in sorted(unknown):
                print(p, file=res_file)
            res_file.close()

        except KeyboardInterrupt as e:
            sorted_programs = sorted(programs.items(), key=operator.itemgetter(1), reverse=True)
            res_file = open(kwargs.get('res_file'), 'w')
            for p in sorted_programs:
                print('%70s\t|\t%s' % (p[0], str(p[1])), file=res_file)
            res_file.close()
            res_file = open(kwargs.get('res_file') + '.unknwn', 'w')

            for p in sorted(unknown):
                print(p, file=res_file)
            res_file.close()