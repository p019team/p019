import os
import glob
from Registry import Registry
import sys
from base import MBase
reload(sys)
sys.setdefaultencoding('ISO-8859-1')


class MPReg(MBase):

    def __init__(self, **kwargs):
        self.logger = kwargs.get('logger')
        self.poickREG = kwargs.get('poickREG')
        self.key = kwargs.get('key')
        self.reg = kwargs.get('reg')
        self.WinListSys = kwargs.get('WinListSys')
        self.ProgRunList = kwargs.get('ProgRunList')
        self.ProgUninstallList = kwargs.get('ProgUninstallList')
        self.OtherList = kwargs.get('OtherList')
        self.ProgList = kwargs.get('ProgList')
        self.base_dir = kwargs.get('base_dir')
    """
    Function searches the path to the file registry
    """
    def findin(self, findfile, base_dir):
        os.chdir(base_dir)
        s = 0
        while s != 5:
            poick = glob.glob(base_dir + findfile)
            if poick == []:
                findfile = '/*' + findfile
                s = s + 1
            else:
                s = 5
                print poick
        if poick == []:
            print "NOT FIND " + findfile[10:]
        return poick
    """
    Function makes a call to the search function
    """
    def SearchFileReg(self, base_dir):
        start = os.getcwd()
        regfilesSoftware = '/System32/config/SOFTWARE'
        poickSoftware = self.findin(regfilesSoftware, base_dir)
        self.poickREG = [poickSoftware]
        os.chdir(start)
    """
    Function performs a search in a directory 'Microsoft\\Windows NT\\CurrentVersion'
    in the registry, and writes a data value in an array
    """
    def PrintWinSys(self, wayfile):
        direct = self.reg.open(wayfile)
        for value in direct.values():
            WinListSys = ""
            if value.value_type() != 0x0003:
                WinListSys = "%s: %s\n" % (value.name(), value.value())
                self.WinListSys.append(WinListSys)
            else:
                WinListSys = value.name() + ": "
                for bite in value.value():
                    if bite != "\00":
                        WinListSys = unicode(WinListSys) + bytes(bite)
                WinListSys = unicode(WinListSys) + "\n"
                self.WinListSys.append(WinListSys)
    """
    Function writes a data value from 'shell\\open\\command' or 'shell\\Open\\command' in an array
    """
    def PrintProg(self, wayfile):
        direct = self.reg.open(wayfile)
        for value in direct.values():
            ProgList = "%s: %s\n" % (value.name(), value.value())
            self.ProgList.append(ProgList)
    """
    Function performs a search in a directory 'Microsoft\\Windows\\CurrentVersion\\Run' or
    'Microsoft\\Windows\\CurrentVersion\\RunOnce' in the registry, and writes a data value
    in an array
    """
    def PrintProgRun(self, wayfile):
        direct = self.reg.open(wayfile)
        ProgRunList = "Directory of Registry: %s\n" % (direct.path())
        self.ProgRunList.append(ProgRunList)
        for value in direct.values():
            ProgRunList =  "%s: %s\n" % (value.name(), value.value())
            self.ProgRunList.append(ProgRunList)
        ProgRunList = "\n"
        self.ProgRunList.append(ProgRunList)
    """
    Function performs a search in a directory 'Microsoft\\Windows\\CurrentVersion\\Uninstall' in the registry,
    and store the following data in an array: "UninstallString", "UninstallPath", "InstallLocation",
    "DisplayVersion", "DisplayName", "InstallSource", "UninstallString".
    """
    def PrintProgUninstall(self, wayfile):
        direct = self.reg.open(wayfile)
        for subkeys in [v for v in direct.subkeys()]:
            if len(subkeys.values()) != 0:
                ProgUninstallList = "New uninstall program: \n"
                self.ProgUninstallList.append(ProgUninstallList)
            for value in [v for v in subkeys.values()
                            if v.name() == "UninstallString" or v.name() == "UninstallPath" or\
                                v.name() == "InstallLocation" or v.name() == "DisplayVersion" or\
                                v.name() == "DisplayName" or v.name() == "InstallSource" or\
                                v.name() == "UninstallString"]:
                ProgUninstallList = "%s: %s\n" % (value.name(), value.value())
                self.ProgUninstallList.append(ProgUninstallList)
            if len(subkeys.values()) != 0:
                ProgUninstallList = "\n"
                self.ProgUninstallList.append(ProgUninstallList)
    """
    Function performs a search  the values in the directory 'Classes' in the registry
    """
    def ProgSearchCl(self,wayfile):
        direct = self.reg.open(wayfile)
        for h in direct.subkeys():
            wayfile = h.path()
            buf = wayfile.find("\\")
            wayfile = wayfile[buf+1:]
            print wayfile
            if wayfile != "Classes\\Interface" and wayfile != "Classes\\Wow6432Node\\Interface"\
                    and wayfile != "Classes\\Wow6432Node\\CLSID" and wayfile != "Classes\\CLSID"\
                    and wayfile != "Classes\\Installer\\Assemblies" and wayfile != "Classes\\Installer\\Components"\
                    and wayfile != "Classes\\Installer\\Dependencies" and wayfile != "Classes\\Installer\\Features"\
                    and wayfile != "Classes\\Installer\\Patches" and wayfile != "Classes\\Installer\\Win32Assemblies"\
                    and wayfile != "Classes\\Installer\\UpgradeCodes" and wayfile != "Classes\\AppID":
                if wayfile.find('shell\\open\\command') != -1 or wayfile.find('shell\\Open\\command') != -1:
                    self.PrintProg(wayfile)
                self.ProgSearchCl(wayfile)
    """
    Function makes a call to the search functions required values
    """
    def ReadFileReg(self):
        wayfile = 'Microsoft\\Windows NT\\CurrentVersion'
        self.PrintWinSys(wayfile)
        wayfile = 'Microsoft\\Windows\\CurrentVersion\\Run'
        self.PrintProgRun(wayfile)
        wayfile = 'Microsoft\\Windows\\CurrentVersion\\RunOnce'
        self.PrintProgRun(wayfile)
        wayfile = 'Microsoft\\Windows\\CurrentVersion\\Uninstall'
        self.PrintProgUninstall(wayfile)
        wayfile = 'Classes'
        self.ProgSearchCl(wayfile)
    """
    The function writes data to a file
    """
    def WriteVal(self):
        output = open('result/win.txt', 'w')
        for WinListSys in self.WinListSys:
            output.write(WinListSys.encode('ISO-8859-1'))
        output.close()
        output = open('result/progrun.txt', 'w')
        for ProgRunList in self.ProgRunList:
            output.write(ProgRunList.encode('ISO-8859-1'))
        output.close()
        output = open('result/progunist.txt', 'w')
        for ProgUninstallList in self.ProgUninstallList:
            output.write(ProgUninstallList.encode('utf-16'))
        output.close()
        output = open('result/prog.txt', 'w')
        for ProgList in self.ProgList:
            output.write(ProgList.encode('ISO-8859-1'))
        output.close()
    """
    The function combining the necessary functional
    """
    def check(self, **kwargs):
        try:
            self.SearchFileReg(self.base_dir)
            fileReg = self.poickREG
            """
            poickSoftware = 0
            """
            for line in fileReg[0]:
                self.reg = Registry.Registry(line)
                self.key = self.reg.root()

                OtherList = 'New Reg File:\n'
                self.OtherList.append(OtherList)
                OtherList = "New Reg File:\n"
                self.ProgRunList.append(OtherList)
                WinListSys = "New Win System:\n"
                self.WinListSys.append(WinListSys)
                ProgUninstallList = "New Reg File:\n"
                self.ProgUninstallList.append(ProgUninstallList)
                ProgList = "New Reg File:\n"
                self.ProgList.append(ProgList)

                self.ReadFileReg()

                self.ProgList = unicode(self.ProgList) + "\n"
                self.OtherList = unicode(self.OtherList) + "\n"
                self.ProgRunList = unicode(self.ProgRunList) + "\n"
                self.WinListSys = unicode(self.WinListSys) + "\n"
                self.ProgUninstallList = unicode(self.ProgUninstallList) + "\n"

                self.WriteVal()

        except KeyboardInterrupt as e:
            self.WriteVal()