__author__ = 'alexander'

from abc import ABCMeta, abstractmethod

'''
Base class for all detection methods
'''
class MBase:
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def check(self, **kwargs):
        pass